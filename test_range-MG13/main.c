/***********************************************************************************************//**
 * \file   main.c
 * \brief  Silicon Labs Empty Example Project
 *
 * This example demonstrates the bare minimum needed for a Blue Gecko C application
 * that allows Over-the-Air Device Firmware Upgrading (OTA DFU). The application
 * starts advertising after boot and restarts advertising after a connection is closed.
 ***************************************************************************************************
 * <b> (C) Copyright 2016 Silicon Labs, http://www.silabs.com</b>
 ***************************************************************************************************
 * This file is licensed under the Silabs License Agreement. See the file
 * "Silabs_License_Agreement.txt" for details. Before using this software for
 * any purpose, you must agree to the terms of that agreement.
 **************************************************************************************************/


#ifndef GENERATION_DONE
#error You must run generate first!
#endif

/* Board headers */
#include "boards.h"
#include "ble-configuration.h"
#include "board_features.h"
#include "platform.h"

/* Bluetooth stack headers */
#include "bg_types.h"
#include "native_gecko.h"
#include "gatt_db.h"
#include "aat.h"

/* Libraries containing default Gecko configuration values */
#include "em_emu.h"
#include "em_cmu.h"
#include "em_rtcc.h"
#ifdef FEATURE_BOARD_DETECTED
#include "bspconfig.h"
#include "pti.h"
#endif

/* Device initialization header */
#include "InitDevice.h"

#ifdef FEATURE_SPI_FLASH
#include "em_usart.h"
#include "mx25flash_spi.h"
#endif /* FEATURE_SPI_FLASH */


/* application specific headers */
#include "app_ui.h"
#include "app_timer.h"


#include <stdio.h>
#include "stdio.h"
#include "retargetserial.h"
#include "gpiointerrupt.h"


/***********************************************************************************************//**
 * @addtogroup Application
 * @{
 **************************************************************************************************/

/***********************************************************************************************//**
 * @addtogroup app
 * @{
 **************************************************************************************************/

/* connection parameters */
#define CONN_INTERVAL_MIN     40      //50ms
#define CONN_INTERVAL_MAX     40      //50ms
#define CONN_SLAVE_LATENCY    0       //no latency
#define CONN_TIMEOUT          100     //1000ms

/* scan parameters */
#define SCAN_INTERVAL  		    32      //20ms  
#define SCAN_WINDOW  		      32      //20ms
#define SCAN_PASSIVE          0

/* advertiser parameters */
#define ADV_INTERVAL_MIN      160     //100ms
#define ADV_INTERVAL_MAX      160     //100ms

#define SIZE_BUFF_ADV_DATA    16
#define SIZE_BUFF_ADV         ( SIZE_BUFF_ADV_DATA + 5 )

/* Number of slaves available for tests */
#define NB_DEVICES_AVAILABLE  15

#ifndef MAX_CONNECTIONS
#define MAX_CONNECTIONS       5
#endif

/* Size of the test frame */
#define TEST_FRAME_SIZE       20

/* Flow MBits/s 1 or 2 */
#define PHY_1M                0x01
#define PHY_2M                0x02
#define PHY_125K              0x04
#define PHY_500K              0x08

/* Number of frame to send for the test */
#define NB_TEST_FRAME         500

/* Timeout for slave scanning */
#define SCAN_SLAVE_TIMEOUT    3000  // 3s

uint8_t bluetooth_stack_heap[DEFAULT_BLUETOOTH_HEAP(MAX_CONNECTIONS)];

#ifdef FEATURE_PTI_SUPPORT
static const RADIO_PTIInit_t ptiInit = RADIO_PTI_INIT;
#endif

  
/* Gecko configuration parameters (see gecko_configuration.h) */
static const gecko_configuration_t config = {
  .config_flags=0,
  .sleep.flags=SLEEP_FLAGS_DEEP_SLEEP_ENABLE,
  .bluetooth.max_connections=MAX_CONNECTIONS,
  .bluetooth.heap=bluetooth_stack_heap,
  .bluetooth.heap_size=sizeof(bluetooth_stack_heap),
  .bluetooth.sleep_clock_accuracy = 100, // ppm
  .gattdb=&bg_gattdb_data,
  .ota.flags=0,
  .ota.device_name_len=3,
  .ota.device_name_ptr="OTA",
  #ifdef FEATURE_PTI_SUPPORT
  .pti = &ptiInit,
  #endif
};

/* the UUID of the ms service */
const char test_service_uuid[16] =
  {
    0xF0, 0x19, 0x21, 0xB4, 0x47, 0x8F, 0xA4, 0xBF,
    0xA1, 0x4F, 0x63, 0xFD, 0xEE, 0xD6, 0x14, 0x1D
  };
/* and its little endian version */
char test_service_uuid_le[16];

/* the UUID of the ms control characteristic */
const char test_control_characteristic_uuid[16] =
  {
    0x63, 0x60, 0x32, 0xe0, 0x37, 0x5e, 0xa4, 0x88, 
    0x53, 0x4e, 0x6d, 0xfb, 0x64, 0x35, 0xbf, 0xf7
  };
//and its little endian version
char test_control_characteristic_uuid_le[16];

/* the UUID of the ms control characteristic */
const char test_data_characteristic_uuid[16] =
  {
    0x53, 0xA1, 0x81, 0x1F, 0x58, 0x2C, 0xD0, 0xA5, 
    0x45, 0x40, 0xFC, 0x34, 0xF3, 0x27, 0x42, 0x98
  };
//and its little endian version
char test_data_characteristic_uuid_le[16];


/* Found MS descriptors */
uint32_t test_gatt_service_handle = 13;
uint16_t test_control_characteristic = 15;
uint16_t test_data_characteristic = 18;


/* Flag for indicating DFU Reset must be performed */
uint8_t boot_to_dfu = 0;


/* connection handling variables for multiple slaves */
uint8  connection_handle_table[MAX_CONNECTIONS];
uint32 service_handle_table[MAX_CONNECTIONS][50];
uint8  active_connections_num;

/*
Enumeration of possible states
*/
typedef enum
{	
  INIT,                     // Init test procedure
  READY,                    // Wait for go test
  START,                    // Test flow
  END,                      // End test flow
  STANDBY,                  // Wait for update param response
  RESTART,                  // Wait for next test sequence
  STOP,
  PRINT,                    // Pause to print full result
}Test_State;

typedef enum 
{
	CON_INIT,                     // Default state
	CON_CONNECT,                  // Connection for the configuration
	CON_FIND_SERVICES,            // Find freelocate services
	CON_FIND_CHARACTERISTICS,     // Find freelocate characteristics
  CON_CONNECTED,                // Connected and ready to transmit 
  CON_UPDATE_PARAM,             // Update connection interval value
  CON_RUN,                      // Nominal mode
  CON_FAIL,                     // Mini System fail
}Master_State;

/*
Enumeration of possible connection mode
*/
typedef enum 
{
  ADV_MODE,
  MASTER_MODE,
  SLAVE_MODE,
}Mode;

typedef struct TestFlowTx_t
{
  Mode          Device_mode;
  uint32_t      Delay_Next_Frame; 
  uint16_t      Phy_led_frequency;
  uint32_t      Phy_led_delay;
  uint16_t      Range_led_frequency;
  uint32_t      Range_led_delay;
  bool          Start;
  Master_State  Con_State;
  uint8_t       Phy;
  int8_t        Rssi;
  int8_t        Rssi_av;
  int8_t        Rssi_av_tab[10];
  bool          Test_rssi;
} Test_Range_t;

Test_Range_t Test_Range;

/* LCD line to display */
char device_mode_line[20]     = "Mode:          ADV\n";  /* Device mode */
char conection_state_line[20] = "Connection:  CLOSE\n";  /* Connection state */
char power_line[20]           = "Tx power:         \n";
char phy_line[20]             = "PHY:          125k\n";  /* PHY */
char rssi_line[20]            = "Rssi:             \n";  /* RSSI */
char line3[20]                = "                  \n";   
char test_state_line[20]      = "Test:         STOP\n";  /* Test State */
char PB0_line[20]             = "PB0:              \n";  /* PB0 function */
char PB1_line[20]             = "PB1:              \n";  /* PB1 function */
char line10[20]               = "                  \n";

/* find the index of a given connection in the connection handle table */
uint8 find_connection_handle(uint8 connection)
{
	uint8 c;
	for (c = 0; c < active_connections_num; c++)
	{
		if (connection_handle_table[c] == connection)
		{
			return c;
		}
	}

	return 0xFF;
}

/* Push button generates external signal event for the stacks */  
void handle_button_push(uint8_t pin)
{
	gecko_external_signal((uint32)pin);
}

/* Return elapsed time in miliseconds */
uint32_t Timeout( uint32_t start_time )
{
  uint32_t time, elapsedtime;
  
  time = RTCC_CounterGet();     
  elapsedtime = ( time - start_time );
  
  return elapsedtime;
}

/* Print 10 lines of the lcd */
void PrintLCD( void )
{
  char text[200];
  
  strcpy( text, device_mode_line );
  strcat( text, conection_state_line );
  strcat( text, power_line );
  strcat( text, phy_line );
  strcat( text, rssi_line );
  strcat( text, line3 ); 
  if( Test_Range.Device_mode == MASTER_MODE )
  {
    strcat( text, test_state_line );
    strcat( text, PB0_line );
    strcat( text, PB1_line );
  }
  appUiWriteString( text );
}

   #ifndef FEATURE_IOEXPANDER
/* Periodically called Display Polarity Inverter Function for the LCD.
   Toggles the the EXTCOMIN signal of the Sharp memory LCD panel, which prevents building up a DC
   bias according to the LCD's datasheet */
static void (*dispPolarityInvert)(void *);
  #endif /* FEATURE_IOEXPANDER */

/**************************************************************************//**
 * @brief   Register a callback function at the given frequency.
 *
 * @param[in] pFunction  Pointer to function that should be called at the
 *                       given frequency.
 * @param[in] argument   Argument to be given to the function.
 * @param[in] frequency  Frequency at which to call function at.
 *
 * @return  0 for successful or
 *         -1 if the requested frequency does not match the RTC frequency.
 *****************************************************************************/
int rtcIntCallbackRegister(void (*pFunction)(void*),
                           void* argument,
                           unsigned int frequency)
{
  #ifndef FEATURE_IOEXPANDER

  dispPolarityInvert =  pFunction;
  /* Start timer with required frequency */
  gecko_cmd_hardware_set_soft_timer(TIMER_MS_2_TIMERTICK(1000 / frequency), DISP_POL_INV_TIMER, false);

  #endif /* FEATURE_IOEXPANDER */

  return 0;
}

/**
 * @brief  Main function  
 */
void main(void)
{
  uint32_t start_scan_time = 0;  
 
  uint8 i, idx_sl, idx_rssi;   
  uint16_t txsize = 0;  
  
  uint8_t flow_data[20] = {  0x00, 0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07, 0x08, 0x09,
                              0x10, 0x11, 0x12, 0x13, 0x14, 0x15, 0x16, 0x17, 0x18, 0x19 };
  
  
  bd_addr slave_address[NB_DEVICES_AVAILABLE] = {   
                                { 0xEE, 0x5A, 0x31, 0x57, 0x0B, 0x00 },   // Starter Kit MG12 1
                                { 0x03, 0x5B, 0x31, 0x57, 0x0B, 0x00 },   // Starter Kit MG12 2
                                { 0xF3, 0x5F, 0x31, 0x57, 0x0B, 0x00 },   // Starter Kit MG12 3
                                { 0x31, 0x5A, 0x31, 0x57, 0x0B, 0x00 },   // Starter Kit MG12 4
                                { 0xDB, 0x30, 0x93, 0x57, 0x0B, 0x00 },   // Starter Kit MG13 
                                { 0x00, 0x32, 0x93, 0x57, 0x0B, 0x00 },   // Starter Kit MG13 
                                { 0x2E, 0x32, 0x93, 0x57, 0x0B, 0x00 },   // Starter Kit MG13 
                                { 0x56, 0x32, 0x93, 0x57, 0x0B, 0x00 },   // Starter Kit MG13 
                                { 0x8A, 0xBF, 0x08, 0x57, 0x0B, 0x00 },   // Freeloctae 3
                                { 0xCF, 0xF2, 0x27, 0x57, 0x0B, 0x00 },   // Freeloctae 4
                                { 0x84, 0xF2, 0x27, 0x57, 0x0B, 0x00 },   // Freeloctae 5
                                { 0x3C, 0x41, 0x1B, 0x57, 0x0B, 0x00 },   // Freeloctae 8
                                { 0xE5, 0xA8, 0x07, 0x57, 0x0B, 0x00 },   // Starter Kit NCP
                                { 0xCF, 0xAC, 0x07, 0x57, 0x0B, 0x00 },   // Starter Kit 33
                                { 0x01, 0xF6, 0x07, 0x57, 0x0B, 0x00 } }; // Starter Kit 49
  
  
  struct gecko_msg_le_gap_open_rsp_t                                        *rsp_open;
  struct gecko_msg_le_gap_set_adv_parameters_rsp_t                          *rsp_param_adv;
  struct gecko_msg_gatt_discover_primary_services_by_uuid_rsp_t             *rsp_serv;
  struct gecko_msg_gatt_discover_characteristics_rsp_t                      *rsp_char;
  struct gecko_msg_gatt_set_characteristic_notification_rsp_t               *rsp_notif;
  struct gecko_msg_gatt_server_send_user_write_response_rsp_t               *rsp_wr_resp;
//  struct gecko_msg_gatt_write_characteristic_value_without_response_rsp_t   *rsp_wr_wresp;;
  struct gecko_msg_le_gap_set_mode_rsp_t                                    *rsp_mode;  
//  struct gecko_msg_le_gap_end_procedure_rsp_t                               *rsp_endproc;
  struct gecko_msg_gatt_write_characteristic_value_rsp_t                    *rsp_wr_char;
//  struct gecko_msg_gatt_server_send_characteristic_notification_rsp_t       *rsp_char_notif;
//  struct gecko_msg_gatt_send_characteristic_confirmation_rsp_t              *rsp_nt_resp;
  struct gecko_msg_gatt_set_max_mtu_rsp_t                                   *rsp_mtu;
  struct gecko_msg_le_gap_set_conn_parameters_rsp_t                         *rsp_param;
  struct gecko_msg_le_gap_set_scan_parameters_rsp_t                         *rsp_scan; 
  struct gecko_msg_le_gap_discover_rsp_t                                    *rsp_disc;
  struct gecko_msg_le_connection_set_phy_rsp_t                              *rsp_phy;
//  struct gecko_msg_le_connection_set_parameters_rsp_t                       *rsp_con_param;
  struct gecko_msg_system_set_tx_power_rsp_t                                *rsp_power;
  struct gecko_msg_le_connection_get_rssi_rsp_t                             *rsp_rssi;
  
	/* generating little endian UUIDs */
	for (i = 0; i < 16 ; i++) test_service_uuid_le[i]                 = test_service_uuid[15-i];
  for (i = 0; i < 16 ; i++) test_control_characteristic_uuid_le[i]  = test_control_characteristic_uuid_le[15-i];
  for (i = 0; i < 16 ; i++) test_data_characteristic_uuid_le[i]     = test_data_characteristic_uuid_le[15-i];

  /* Initialize peripherals */
  enter_DefaultMode_from_RESET();

  RETARGET_SerialInit();

  /* Initialize stack */
  gecko_init(&config);  
  
  // Test HW init
  hwInit();
    /* Unique device ID */
  uint16_t devId;
  struct gecko_msg_system_get_bt_address_rsp_t* btAddr;
  char devName[APP_DEVNAME_LEN + 1];

  /* Init device name */
  /* Get the unique device ID */

  /* Create the device name based on the 16-bit device ID */
  btAddr = gecko_cmd_system_get_bt_address();
  devId = *((uint16*)(btAddr->address.addr));

  /* Initialize graphics. */
  appUiInit(devId);

  
  
  printf("\r\n\r*** StarterKit started *** \n\r\r");
  
  Test_Range.Device_mode        = ADV_MODE;
  Test_Range.Con_State          = CON_INIT;
  Test_Range.Delay_Next_Frame   = 0;
  Test_Range.Start              = 0;
  Test_Range.Test_rssi          = 0;
  Test_Range.Phy                = PHY_125K;   // Keep these two values
  Test_Range.Phy_led_frequency  = 125;     // in harmony
  Test_Range.Rssi               = 0;  
  Test_Range.Phy_led_delay     = RTCC_CounterGet();
  Test_Range.Range_led_delay   = RTCC_CounterGet(); 
  
  GPIO_PinOutClear( gpioPortF, 4 );        
  
  while (1) 
  {
    struct gecko_cmd_packet* evt;    
    
    /* Check for stack event. */
    evt = gecko_peek_event();
    
    /* Master mode? */
    if( Test_Range.Device_mode == MASTER_MODE )
    {
      /* Check if the timeout for the slave scanning is over */
      if( ( start_scan_time != 0 ) && ( SCAN_SLAVE_TIMEOUT <= Timeout( start_scan_time ) ) )
      {
        /* No slave available, the device is now setup as slave and sender (control link ) */ 
        Test_Range.Device_mode = SLAVE_MODE;
        printf( "No slave available, switch to slave mode\n\r" );
        
        snprintf( device_mode_line, sizeof(device_mode_line), "Mode:        SLAVE\n" );
        PrintLCD();
        
        /* Stop scanning */
        gecko_cmd_le_gap_end_procedure();
        
        /* Set adv parameters */
        rsp_param_adv = gecko_cmd_le_gap_set_adv_parameters( ADV_INTERVAL_MIN, ADV_INTERVAL_MAX, 7 );    
        if( rsp_param_adv->result )
          printf( "Error, set adv param, %x\n\r", rsp_mode->result );
        else
          printf( "Set adv parameters OK \n\r" );
                
        gecko_cmd_sm_set_bondable_mode( 1 );
        
        /* Pairing only, no bonding */
        gecko_cmd_sm_configure( 4, sm_io_capability_noinputnooutput );        
        
        /* Start advertising for the future master */
        rsp_mode = gecko_cmd_le_gap_set_mode ( le_gap_general_discoverable, le_gap_undirected_connectable );          
        if( rsp_mode->result )
          printf( "Error, set mode failed, %x\n\r", rsp_mode->result );
        else
          printf( "Start advertising \n\r" );
        
        /* Change state */
        Test_Range.Con_State = CON_CONNECT;
        
        /* Stop timer */  
        start_scan_time = 0;        
      }           
      
      if( Test_Range.Start )
      {
        /* Wait 250 ms before next frame send */
        if( Timeout( Test_Range.Delay_Next_Frame ) >= 200 )
        {  
          /* Send frame for the test range */   
          rsp_wr_char = gecko_cmd_gatt_write_characteristic_value( connection_handle_table[active_connections_num], test_control_characteristic, 20, flow_data);  
          /* Send KO */
          if( rsp_wr_char->result )
          {
            printf( "Error, send failed, %x\n\r", rsp_wr_char->result ); 
            snprintf( rssi_line, sizeof(rssi_line), "Error, send failed\n" );
          }
          /* Send OK */
          else
          {           
            /* Get rssi */
            rsp_rssi = gecko_cmd_le_connection_get_rssi( connection_handle_table[active_connections_num] );
            if( rsp_rssi->result )
              printf( "Error, get rssi failed, %x\n\r", rsp_rssi->result ); 
          }
              
          Test_Range.Delay_Next_Frame = RTCC_CounterGet();             
        }
        
        switch( ( ~Test_Range.Rssi ) / 10 )
        {
          case 0:
            Test_Range.Range_led_frequency = 25;
            break;
            
          case 1:
            Test_Range.Range_led_frequency = 50;
            break;
            
          case 2:
            Test_Range.Range_led_frequency = 100;
            break;
            
          case 3:
            Test_Range.Range_led_frequency = 175;
            break;
            
          case 4:
            Test_Range.Range_led_frequency = 275;
            break;
            
          case 5:
            Test_Range.Range_led_frequency = 400;
            break;
            
          case 6:
            Test_Range.Range_led_frequency = 550;
            break;
            
          case 7:
            Test_Range.Range_led_frequency = 725;
            break;
            
          case 8:
            Test_Range.Range_led_frequency = 925;
            break;
            
          case 9:
            Test_Range.Range_led_frequency = 1150;
            break;
            
          case 10:
            Test_Range.Range_led_frequency = 1400;
            break;
            
          case 11:
            Test_Range.Range_led_frequency = 1675;
            break;
            
          case 12:
            Test_Range.Range_led_frequency = 1975;
            break;
        }
        /* Range led indicator */
        if( Timeout( Test_Range.Range_led_delay ) >= Test_Range.Range_led_frequency )
        {      
          /* Blinking according to range level*/
          GPIO_PinOutToggle( gpioPortF, 4 );           
          Test_Range.Range_led_delay = RTCC_CounterGet();  
        }        
      }      
      
      /* Phy led indicator */
      if( Timeout( Test_Range.Phy_led_delay ) >= Test_Range.Phy_led_frequency )
      {      
        /* Blinking according to phy level */
        GPIO_PinOutToggle( gpioPortF, 5 );
        Test_Range.Phy_led_delay = RTCC_CounterGet();  
      }                       
                
      /* this event is triggered by gecko_external_signal() *
      * in our case when a button is pushed                */
      if( BGLIB_MSG_ID( evt->header ) == gecko_evt_system_external_signal_id )
      {
        /* BP0 - Change Phy*/
        if( evt->data.evt_system_external_signal.extsignals == 8 )
        {            
            /* Changement phy */
          switch( Test_Range.Phy )               
          {
            case PHY_1M:
              Test_Range.Phy_led_frequency = 2000;
              Test_Range.Phy = PHY_2M; 
              break;
              
            case PHY_2M:
              Test_Range.Phy_led_frequency = 125;
              Test_Range.Phy = PHY_125K;
              break;
              
            case PHY_125K:
              Test_Range.Phy_led_frequency = 500;
              Test_Range.Phy = PHY_500K;
              break;
              
            case PHY_500K:
              Test_Range.Phy_led_frequency = 1000;
              Test_Range.Phy = PHY_1M;
              break;
              
            default:
              Test_Range.Phy = PHY_1M;
              break;
          }
              
          rsp_phy = gecko_cmd_le_connection_set_phy( connection_handle_table[active_connections_num], Test_Range.Phy ); 
          if( rsp_phy->result )
            printf( "Error, update phy failed, %x\n\r", rsp_phy->result ); 
          else
            printf( "Update phy value...\n\r" );
          
          Test_Range.Phy_led_delay = RTCC_CounterGet();   
        }

        /* BP1 - Start test */
        if( evt->data.evt_system_external_signal.extsignals == 4 )
        {
          if( Test_Range.Start )
          {
            Test_Range.Start = 0;
            printf( "***** Pause *****\n\r" );
            snprintf( test_state_line, sizeof(test_state_line), "Test:         STOP\n" );
            snprintf( PB0_line, sizeof(PB0_line), "PB0:    Start Test\n" ); 
            PrintLCD();
          }
          else
          {
            Test_Range.Start = 1;
            Test_Range.Test_rssi = 1;
            
            printf( "***** Start *****\n\r" );
            snprintf( test_state_line, sizeof(test_state_line), "Test:          RUN\n" );
            snprintf( PB0_line, sizeof(PB0_line), "PB0:     Stop Test\n" ); 
            PrintLCD();
          }
        }
      } 
    }
    else if( Test_Range.Device_mode == SLAVE_MODE )
    {      
      /* Start test rssi */
      if( Test_Range.Test_rssi )
      {
        switch( ( ~Test_Range.Rssi ) / 10 )
        {
          case 0:
            Test_Range.Range_led_frequency = 25;
            break;
            
          case 1:
            Test_Range.Range_led_frequency = 50;
            break;
            
          case 2:
            Test_Range.Range_led_frequency = 100;
            break;
            
          case 3:
            Test_Range.Range_led_frequency = 175;
            break;
            
          case 4:
            Test_Range.Range_led_frequency = 275;
            break;
            
          case 5:
            Test_Range.Range_led_frequency = 400;
            break;
            
          case 6:
            Test_Range.Range_led_frequency = 550;
            break;
            
          case 7:
            Test_Range.Range_led_frequency = 725;
            break;
            
          case 8:
            Test_Range.Range_led_frequency = 925;
            break;
            
          case 9:
            Test_Range.Range_led_frequency = 1150;
            break;
            
          case 10:
            Test_Range.Range_led_frequency = 1400;
            break;
            
          case 11:
            Test_Range.Range_led_frequency = 1675;
            break;
            
          case 12:
            Test_Range.Range_led_frequency = 1975;
            break;
        }
        /* Range led indicator */
        if( Timeout( Test_Range.Range_led_delay ) >= Test_Range.Range_led_frequency )
        {      
          /* Blinking according to range level*/
          GPIO_PinOutToggle( gpioPortF, 4 );           
          Test_Range.Range_led_delay = RTCC_CounterGet();  
        }
      }           
      
      /* Phy led indicator */
      if( Timeout( Test_Range.Phy_led_delay ) >= Test_Range.Phy_led_frequency )
      {      
        /* Blinking according to phy level */
        GPIO_PinOutToggle( gpioPortF, 5 );
        Test_Range.Phy_led_delay = RTCC_CounterGet();  
      }
    }
   
    
    switch( Test_Range.Device_mode )
    {
      /*****************************************    MASTER MODE    *****************************************/
      case MASTER_MODE:
        switch( BGLIB_MSG_ID( evt->header ) )
        {            
          /* This event is generated when an advertisement packet or a scan response is received from a slave */
          case gecko_evt_le_gap_scan_response_id:   
            /* Check if the device is authorized */
            for( idx_sl = 0; idx_sl < NB_DEVICES_AVAILABLE; idx_sl++ )
            {
              /* Switch ? */
              if( memcmp( &evt->data.evt_le_gap_scan_response.address, &slave_address[idx_sl], 6 ) == 0 )
              {  
                /* Stop scanning */              
                gecko_cmd_le_gap_end_procedure();
                  
                /* Open connection */
                rsp_open = gecko_cmd_le_gap_open( evt->data.evt_le_gap_scan_response.address, evt->data.evt_le_gap_scan_response.address_type );
                if( rsp_open->result )
                  printf( "Error, open connection failed, %x", rsp_open->result ); 
                          
                /* Stop timer */
                start_scan_time = 0;
              }
            }
            break; /* End gecko_evt_le_gap_scan_response_id */
            
           /* This event is generated when a new connection is established */
          case gecko_evt_le_connection_opened_id:            
            printf( "Connection opened\n\r" );            
            printf( "PDU fram size: %d\n\r", txsize ); 
            snprintf( conection_state_line, sizeof(conection_state_line), "Connection:   OPEN\n" );
            if( !Test_Range.Start )
            {
                snprintf( PB0_line, sizeof(PB0_line), "PB0:    Start Test\n" ); 
            }
            snprintf( PB1_line, sizeof(PB1_line), "PB1:    Update phy\n" ); 
            PrintLCD();
            
            /* Save the connection handle */
            connection_handle_table[active_connections_num] = evt->data.evt_le_connection_opened.connection;   
            
            /* Stop advertising */
            rsp_mode = gecko_cmd_le_gap_set_mode ( le_gap_non_discoverable, le_gap_non_connectable );
            if( rsp_mode->result )
              printf( "Error, set mode failed, %x\n\r", rsp_mode->result );
            else
              printf( "Connection opened, stop advertising \n\r" );
            
            /* Update phy level */
            rsp_phy = gecko_cmd_le_connection_set_phy( connection_handle_table[active_connections_num], Test_Range.Phy ); 
            if( rsp_phy->result )
              printf( "Error, update phy failed, %x\n\r", rsp_phy->result ); 
            else
              printf( "Update phy value...\n\r" );    
            
            /* discover ms service on the slave device */
            rsp_serv = gecko_cmd_gatt_discover_primary_services_by_uuid( evt->data.evt_le_connection_opened.connection, sizeof( test_service_uuid ), (const uint8*)test_service_uuid );
            if( rsp_serv->result )
              printf( "Error, services discovery failed, %x\n\r", rsp_serv->result );
            else
              printf( "Services discovery started..." );
            
            Test_Range.Con_State = CON_FIND_SERVICES;
            break;  /* End gecko_evt_le_connection_opened_id */
                           
          /* this event is generated when a new service is discovered */
          case gecko_evt_gatt_service_id:
            /* Check if the service UUID is the one of the msini-system */
            if( memcmp( evt->data.evt_gatt_service.uuid.data, test_service_uuid, sizeof( test_service_uuid ) ) == 0 )
            {            
              test_gatt_service_handle = evt->data.evt_gatt_service.service;
              printf( "OK\n\r- Service handle: %d\n\r", test_gatt_service_handle );
            }
            else
            {
              /* No, error */
              Test_Range.Con_State = CON_FAIL;
              printf( "KO\n\r" );
            }
            break;  /* End gecko_evt_gatt_service_id */
            
            
          /* This event is generated when a new characteristic is discovered */
					case gecko_evt_gatt_characteristic_id:
						if( ( evt->data.evt_gatt_characteristic.uuid.len == sizeof( test_control_characteristic_uuid ) ) &&
							( !memcmp( evt->data.evt_gatt_characteristic.uuid.data, test_control_characteristic_uuid, sizeof( test_control_characteristic_uuid ) ) ) )
						{
							test_control_characteristic = evt->data.evt_gatt_characteristic.characteristic;
              printf( "OK\n\r- Control handle:%d\n\r", test_control_characteristic );
						}
						if( ( evt->data.evt_gatt_characteristic.uuid.len == sizeof( test_data_characteristic_uuid ) ) &&
							( !memcmp( evt->data.evt_gatt_characteristic.uuid.data, test_data_characteristic_uuid, sizeof( test_data_characteristic_uuid ) ) ) )
						{
							test_data_characteristic = evt->data.evt_gatt_characteristic.characteristic;
              printf( "- Data handle:%d\n\r", test_data_characteristic );
						}
						break;  /* End gecko_evt_gatt_characteristic_id */
            
          /* This event is generated for various procedure completions */
          case gecko_evt_gatt_procedure_completed_id:
            switch( Test_Range.Con_State )
            {
              case CON_FIND_SERVICES:                
                /* Discover mini-system characteristics */
                rsp_char = gecko_cmd_gatt_discover_characteristics( evt->data.evt_gatt_service.connection, test_gatt_service_handle );
                if( rsp_char->result )
                  printf( "Error, characteristics discovery failed, %x\n\r", rsp_char->result );
                else
                {
                  printf( "Characteristics discovery started..." );                
                }
                
                test_control_characteristic = 0xFFFF;
                test_data_characteristic = 0xFFFF;
                
                /* Find service characteristics */
                Test_Range.Con_State = CON_FIND_CHARACTERISTICS;
                break;  /* End TEST_FIND_SERVICES */
                
              case CON_FIND_CHARACTERISTICS:
                /* UUID valid? */
                if( ( test_control_characteristic != 0xFFFF ) &&
                    ( test_data_characteristic != 0xFFFF ) )
                {                  
                  /* Register to Server notifications/indications */
                  rsp_notif = gecko_cmd_gatt_set_characteristic_notification( evt->data.evt_gatt_service.connection, test_control_characteristic, 1);
                  if( rsp_notif->result )
                    printf( "Error, notification registration failed, %x\n\r", rsp_notif->result );
                  else
                    printf( "Notification registration started\n\r" );
                  
                  /* Start test */
                  Test_Range.Con_State = CON_CONNECTED;
                }
                else
                {
                  Test_Range.Con_State = CON_FAIL;
                }
                break;  /* End TEST_FIND_CHARACTERISTICS */
                
              case CON_CONNECTED:  
                /* Send frame for the test range */   
                rsp_wr_char = gecko_cmd_gatt_write_characteristic_value( evt->data.evt_gatt_service.connection, test_control_characteristic, 20, flow_data);  
                if( rsp_wr_char->result )
                  printf( "Error, send failed, %x\n\r", rsp_wr_char->result ); 
                else
                  printf( "Write frame\n\r" );  
                
                /* Nominal mode */
                Test_Range.Con_State = CON_RUN;
                break; 
                
              default:
                break;
            }
            break;  /* End gecko_evt_gatt_procedure_completed_id */
            
          /* Confirmation update phy value */
          case gecko_evt_le_connection_phy_status_id:
            printf( "Phy update... " );  
            if( evt->data.evt_le_connection_phy_status.phy == Test_Range.Phy )
            {              
              printf( "OK, PHY = %d\n\r", evt->data.evt_le_connection_phy_status.phy ); 
              switch( evt->data.evt_le_connection_phy_status.phy )
              {
                case PHY_1M:
                  snprintf( phy_line, sizeof(phy_line), "PHY:            1M\n" );
                  break;
                  
                case PHY_2M:
                  snprintf( phy_line, sizeof(phy_line), "PHY:            2M\n" );
                  break;
                  
                case PHY_125K:
                  snprintf( phy_line, sizeof(phy_line), "PHY:          125k\n" );
                  break;
                  
                case PHY_500K:
                  snprintf( phy_line, sizeof(phy_line), "PHY:          500k\n" );
                  break;
              }
              PrintLCD();
            }
            else
            {
              printf( "KO, PHY = %d\n\r", evt->data.evt_le_connection_phy_status.phy );   
              snprintf( phy_line, sizeof(phy_line), "PHY:  Error update\n" );
              PrintLCD();
            }
            break;       
            
          /* Get rssi */
          case gecko_evt_le_connection_rssi_id:
            Test_Range.Rssi = evt->data.evt_le_connection_rssi.rssi;
            
            for( idx_rssi = 0; idx_rssi < 9; idx_rssi ++ )
            {
              Test_Range.Rssi_av_tab[idx_rssi] = Test_Range.Rssi_av_tab[idx_rssi+1];
            }
            Test_Range.Rssi_av_tab[idx_rssi] = Test_Range.Rssi;
            
            Test_Range.Rssi_av = (  Test_Range.Rssi_av_tab[0]
                                  + Test_Range.Rssi_av_tab[1]
                                  + Test_Range.Rssi_av_tab[2]
                                  + Test_Range.Rssi_av_tab[3]
                                  + Test_Range.Rssi_av_tab[4]
                                  + Test_Range.Rssi_av_tab[5]
                                  + Test_Range.Rssi_av_tab[6]
                                  + Test_Range.Rssi_av_tab[7]
                                  + Test_Range.Rssi_av_tab[8]
                                  + Test_Range.Rssi_av_tab[9] ) / 10;
            
            printf( "Rssi: %d dBm\n\r", Test_Range.Rssi_av );
            snprintf( rssi_line, sizeof(rssi_line), "                    " );
            snprintf( rssi_line, sizeof(rssi_line), "Rssi:      %d dBm\n", Test_Range.Rssi_av );
            PrintLCD();
            break;
            
//          case gecko_evt_le_connection_parameters_id:
//            /* Send frame to test ranges */   
//            rsp_wr_char = gecko_cmd_gatt_write_characteristic_value( connection_handle_table[active_connections_num], test_control_characteristic, 20, flow_data);  
//            if( rsp_wr_char->result )
//              printf( "Error, send failed, %x\n\r", rsp_wr_char->result );
//            else
//              printf( "Pouet \n\r" );
//            break;
          
          /* This event is generated when a connection is dropped */
          case gecko_evt_le_connection_closed_id:            
            /* Switch advertising mode */            
            printf( "Close connection, reason: " );
            if( evt->data.evt_le_connection_closed.reason == 0x0208 )
              printf( "connection timeout \n\r" );
            else                       
              printf( "%x\n\r", evt->data.evt_le_connection_closed.reason );
                        
            snprintf( conection_state_line, sizeof(conection_state_line), "Connection:  CLOSE\n" );
            PrintLCD();
              
            /* Reset test mode */
            Test_Range.Con_State = CON_CONNECT;
            
            Test_Range.Start = 0;
            
            /* Blinking according to range level*/
            GPIO_PinOutSet( gpioPortF, 4 );        
            GPIO_PinOutSet( gpioPortF, 5 );        
        
            /* Start scanning for slaves */
            rsp_disc = gecko_cmd_le_gap_discover( le_gap_discover_generic );
            if( rsp_disc->result )
              printf( "Error, start scan failed, %x\n\r", rsp_disc->result );
            else
              printf( "Start discover... OK\n\r" );
          break;  // End gecko_evt_le_connection_closed_id
            
          default:
            break;
        }
        break;  /* End MASTER_MODE: */
      /*****************************************    MASTER MODE    *****************************************/
            
      /*****************************************    SLAVE MODE     *****************************************/
      case SLAVE_MODE:
        switch( BGLIB_MSG_ID( evt->header ) )
        {    
          /* this event is generated when a new connection is established */
          case gecko_evt_le_connection_opened_id:
            /* Stop advertising */    
            rsp_mode = gecko_cmd_le_gap_set_mode ( le_gap_non_discoverable, le_gap_non_connectable );
            if( rsp_mode->result )
              printf( "Error, set mode failed, %x\n\r", rsp_mode->result );
            else
              printf( "Connection opened, stop advertising \n\r" );   
            
            snprintf( conection_state_line, sizeof(conection_state_line), "Connection:   OPEN\n" );
            snprintf( PB1_line, sizeof(PB1_line), "PB1:    Update phy\n" ); 
            PrintLCD();
            
            /* Save the connection handle */
            connection_handle_table[active_connections_num] = evt->data.evt_le_connection_opened.connection;   
            
            /* Link is established now!
             Security Manager - Enhance the security of the connection to sart encryption */
//            struct gecko_msg_sm_increase_security_rsp_t *security_rsp;
//            security_rsp = gecko_cmd_sm_increase_security( evt->data.evt_le_connection_opened.connection );       
//            if( security_rsp->result )
//              printf( "Error, increase security failed, %x\n\r", security_rsp->result );
//            else
//              printf( "Security Level Up! \n\r" );     
            
//            /* Update phy level */
//            rsp_phy = gecko_cmd_le_connection_set_phy( evt->data.evt_le_connection_opened.connection, Test_Range.Phy ); 
//            if( rsp_phy->result )
//              printf( "Error, update phy failed, %x\n\r", rsp_phy->result );               
            break;  /* End gecko_evt_le_connection_opened_id */
            
          /* This event is generated when the value of a characteristic was received */
          case gecko_evt_gatt_server_user_write_request_id:
            if( evt->data.evt_gatt_characteristic_value.characteristic == test_control_characteristic )
            {
              /* Write response */
              rsp_wr_resp = gecko_cmd_gatt_server_send_user_write_response ( evt->data.evt_gatt_service.connection, test_control_characteristic, 0 );   
              if( rsp_wr_resp->result )
                printf( "Error, user write response failed, %x\n\r", rsp_wr_resp->result );
              
              /* Get rssi */
              rsp_rssi = gecko_cmd_le_connection_get_rssi( connection_handle_table[active_connections_num] );
              if( rsp_rssi->result )
                printf( "Error, get rssi failed, %x\n\r", rsp_rssi->result ); 
              
              Test_Range.Test_rssi = 1;
            }  
            else if( evt->data.evt_gatt_characteristic_value.characteristic == test_data_characteristic )
            {
            }
            else
            {
              printf( "Wrong characteristic received\n\r" );
            }
            break; /* End evt_gatt_characteristic_value */
            
          /* Get rssi */
          case gecko_evt_le_connection_rssi_id:
            Test_Range.Rssi = evt->data.evt_le_connection_rssi.rssi;
            
            for( idx_rssi = 0; idx_rssi < 9; idx_rssi ++ )
            {
              Test_Range.Rssi_av_tab[idx_rssi] = Test_Range.Rssi_av_tab[idx_rssi+1];
            }
            Test_Range.Rssi_av_tab[idx_rssi] = Test_Range.Rssi;
            
            Test_Range.Rssi_av = (  Test_Range.Rssi_av_tab[0]
                                  + Test_Range.Rssi_av_tab[1]
                                  + Test_Range.Rssi_av_tab[2]
                                  + Test_Range.Rssi_av_tab[3]
                                  + Test_Range.Rssi_av_tab[4]
                                  + Test_Range.Rssi_av_tab[5]
                                  + Test_Range.Rssi_av_tab[6]
                                  + Test_Range.Rssi_av_tab[7]
                                  + Test_Range.Rssi_av_tab[8]
                                  + Test_Range.Rssi_av_tab[9] ) / 10;
            
            printf( "Rssi: %d dBm\n\r", Test_Range.Rssi_av );
            snprintf( rssi_line, sizeof(rssi_line), "                    " );
            snprintf( rssi_line, sizeof(rssi_line), "Rssi:      %d dBm\n", Test_Range.Rssi_av );
            PrintLCD();
            break;
            
          /* This event is generated when a connection is dropped */
          case gecko_evt_le_connection_closed_id:                 
            printf( "Close connection, reason: " );
            if( evt->data.evt_le_connection_closed.reason == 0x0208 )
              printf( "connection timeout \n\r" );
            else                       
              printf( "%x\n\r", evt->data.evt_le_connection_closed.reason );
            
            snprintf( conection_state_line, sizeof(conection_state_line), "Connection:  CLOSE\n" );
            PrintLCD();
           
            /* Blinking according to range level*/
            GPIO_PinOutSet( gpioPortF, 4 );        
            GPIO_PinOutSet( gpioPortF, 5 ); 
            Test_Range.Test_rssi = 0;
            
            /* Restart advertising for the future master */
            rsp_mode = gecko_cmd_le_gap_set_mode ( le_gap_general_discoverable, le_gap_undirected_connectable );          
            if( rsp_mode->result )
              printf( "Error, set mode failed, %x\n\r", rsp_mode->result );
            else
              printf( "Start advertising \n\r" );
            break;  /* End gecko_evt_le_connection_closed_id */
            
          /* Confirmation update phy value */
          case gecko_evt_le_connection_phy_status_id:
            Test_Range.Phy = evt->data.evt_le_connection_phy_status.phy; 

            printf( " Phy update, PHY = %d\n\r", Test_Range.Phy ); 
            
            /* Changement phy */
            switch( Test_Range.Phy )               
            {
              case PHY_1M:
                snprintf( phy_line, sizeof(phy_line), "PHY:            1M\n" );
                break;
                
              case PHY_2M:
                snprintf( phy_line, sizeof(phy_line), "PHY:           2M \n" );
                break;
                
              case PHY_125K:
                snprintf( phy_line, sizeof(phy_line), "PHY:          125k\n" );
                break;
                
              case PHY_500K:
                snprintf( phy_line, sizeof(phy_line), "PHY:          500k\n" );
              break;
            }
            PrintLCD();
            break;
            
          default:
            break;
        }
        break;  /* End SLAVE_MODE:*/
      /*****************************************    SLAVE MODE     *****************************************/
        
      /*****************************************     ADV MODE      *****************************************/
      case ADV_MODE:
        switch( BGLIB_MSG_ID( evt->header ) )
        {     
          /* This boot event is generated when the system boots up after reset. */
          case gecko_evt_system_boot_id:/* Set scan parameters */
            rsp_mtu = gecko_cmd_gatt_set_max_mtu(247);
            if( rsp_mtu->result )
              printf( "Error, set MTU failed, %x\n\r", rsp_mtu->result );
            else
              printf( "Set MTU... OK\n\r" );            
            
            /* Set max power +10dBm */
            rsp_power = gecko_cmd_system_set_tx_power( 100 );
            if( rsp_power->set_power != 100 )
              printf( "Error, set power failed, %x\n\r", rsp_power->set_power );
            else
              printf( "Set max power... OK, %d \n\r", rsp_power->set_power );
                        
            snprintf( power_line, sizeof(power_line), "Tx power:    %ddBm\n", rsp_power->set_power/10 ); 
              
            rsp_scan = gecko_cmd_le_gap_set_scan_parameters( SCAN_INTERVAL, SCAN_WINDOW, SCAN_PASSIVE );
            if( rsp_scan->result )
              printf( "Error, set scan failed, %x\n\r", rsp_scan->result );
            else
              printf( "Set scan param... OK\n\r" );
    
            rsp_param = gecko_cmd_le_gap_set_conn_parameters( CONN_INTERVAL_MIN, CONN_INTERVAL_MAX, CONN_SLAVE_LATENCY, CONN_TIMEOUT );
            if( rsp_param->result )
              printf( "Error, set conn param failed, %x\n\r", rsp_param->result );
            else
              printf( "Set connection param... OK\n\r" );            
                            
            /* Start scanning for slaves */
            rsp_disc = gecko_cmd_le_gap_discover( le_gap_discover_generic );
            if( rsp_disc->result )
              printf( "Error, start scan failed, %x\n\r", rsp_disc->result );
            else
              printf( "Start discover... OK\n\r" );
            
            /* Enter Master mode */
            Test_Range.Device_mode = MASTER_MODE;
            
            printf( "Master mode\n\r" );        
            snprintf( device_mode_line, sizeof(device_mode_line), "Mode:       MASTER\n" );
            PrintLCD();
        
            /* Start timer in case of no slave available */      
            start_scan_time = RTCC_CounterGet();
            break;
                       
          /* this event is triggered by gecko_external_signal() *
          * in our case when a button is pushed                */
          case gecko_evt_system_external_signal_id:            
            /* BP0 */
            if( evt->data.evt_system_external_signal.extsignals == 6 )
            {
              
            }

            /* BP1 */
            if( evt->data.evt_system_external_signal.extsignals == 7 )
            {
            }
            break;  /* End gecko_evt_system_external_signal_id */
        }
        break;  /* End ADV_MODE */        
      /*****************************************     ADV MODE      *****************************************/
      
      default:
        break;
    } /* End switch */
  } /* End while */
} 

